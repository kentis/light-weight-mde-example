include ./secrets.sh
SHELL=/bin/bash

clean:
	@rm -rf out
	@rm -f ENDPOINT
	@cd generator && gradle clean

build_generator: clean
	cd generator && gradle build shadowJar

generate: build_generator
	java -jar generator/build/libs/generator-all.jar -o ./out models/MinimalPrototypeModel.yaml

build_serverless: generate
	cd out && npm i

test_local: build_serverless
	@echo "------------ local test -----------------"
	cd out && \
	serverless invoke local --function app -p ../data.json
	@echo "-----------------------------------------"

deploy: build_serverless
	cd out && \
	serverless remove || true && \
	serverless deploy && \
	serverless info -v | grep ServiceEndpoint | cut -f 2- -d : | xargs > ../ENDPOINT

test_aws:
	@echo "------------ aws test -----------------"
	read URL < ./ENDPOINT; curl -H "Content-Type: application/json" -X POST $${URL}/score -d '{"Industry": "other", "Estimated_annual_sale": "500 000 - 1 000 000 NOK"}'
	@echo
	@echo "-----------------------------------------"

remove: build_serverless
	cd out && \
	serverless remove || true