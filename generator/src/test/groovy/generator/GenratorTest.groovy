/*
 * This Spock specification was generated by the Gradle 'init' task.
 */
package generator

import spock.lang.Specification

class GeneratorTest extends Specification {
    def "can generate package-lock"() {
        
        setup:
        def parser = new Parser()
        def generator = new Generator()
        
        when:
        def result = generator.generatePackageLock()
        
        then:
        result != null
        result.startsWith("{")
        result.trim().endsWith("}")
    }

    def "can generate index"() {
        
        setup:
        def parser = new Parser()
        def generator = new Generator()
        def scoring = [components: [[name:"a",type: "ValueMap", values:["a":1, "b": 2]]
                  , [name:"b",type: "ValueMap", values:["a":2, "b":1]]]
                  , aggregator: '']
        when:
        def result = generator.generateIndex(scoring)
        
        then:
        println result
        result != null
        result.startsWith("const")
        result.trim().endsWith(";")
    }

    def "can generate component"() {
        
        setup:
        def parser = new Parser()
        def generator = new Generator()
        def component = [name:"a",type: "ValueMap", values:["a":1, "b": 2]]
        when:
        def result = generator.generateComponent(component)
        
        then:
        println result
        result != null
        result.startsWith("exports.")
        result.trim().endsWith("};")
    }

    def "can generate aggregator"() {
        
        setup:
        def parser = new Parser()
        def generator = new Generator()
        def scoring = [components: [[name:"a",type: "ValueMap", weight: 2.5, values:["a":1, "b": 2]]
                  , [name:"b",type: "ValueMap", weight: 5, values:["a":2, "b":1]]]
                  , aggregator: [type: "WeightedAverageAccumulator"]]
        when:
        def result = generator.generateAggregator(scoring)
        
        then:
        println result
        result != null
        result.startsWith("export")
        result.trim().endsWith("};")
    }
}
