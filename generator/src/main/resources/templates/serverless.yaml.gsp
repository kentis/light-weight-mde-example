service: sample-scoring-application

provider:
  name: aws
  runtime: nodejs10.x
  stage: dev
  region: us-east-1

package:
  include:
    - lib/**

functions:
  app:
    handler: index.handler
    events:
      - http: 
          method: any
          path: /
          cors: true
      - http: 
          method: any
          path: /{proxy+}
          cors: true
