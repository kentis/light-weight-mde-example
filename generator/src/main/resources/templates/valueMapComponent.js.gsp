exports.score = function (model) {
  var value = 1;
  switch(model.${name.replaceAll(" ", "_")}){
    
    <% values.each{k, v -> %>
        case "${k}":
            value = ${v};
            break;
    <% } %>
  }
  return (value - 1) / 2;
}; 