exports.score = function (model) {
  let dividend = typeof model.${dividend} === 'object' ? model.${dividend}.score : model.${dividend};
  let divisor = typeof model.${divisor} === 'object' ? model.${divisor}.score : model.${divisor};
  let ratio = dividend / divisor;
  <% if(binding.hasVariable("limit")) { %>
  return {score: ratio, valid: ratio >= ${limit}, input: [model.${dividend}, model.${divisor}]};
  <% } else if(binding.hasVariable("upper_limit")) { %>
  return {score: ratio, valid: ratio <= ${upper_limit}};
  <% } %>
};