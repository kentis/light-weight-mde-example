exports.score = function (model) {
  var value = 0;
  <% inputs.each{ i -> %>
    value += model.${i};
  <% } %>
  return {"score": value, valid: true};
};