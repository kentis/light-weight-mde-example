exports.aggregate = function(scores){
    let aggregated = 0;
    const maxScore = ${components.weight.sum()};
    <% components.each { %>
    aggregated += scores.${it._name} * ${it.weight};
    <% } %>
    return (aggregated / maxScore) * 100;
};