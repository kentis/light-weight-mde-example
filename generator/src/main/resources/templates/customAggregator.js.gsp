exports.aggregate = function(scores){
    let score = 0;
    let valid = true;
    <% components.each { %>
    let ${it._name} = scores.${it._name}.score;
    <% } %>

    valid = valid <% components.each { %> && scores.${it.name}.valid <% } %>;
    score = ${aggregator.scoring};
    

    return {score: score, valid: valid && score > ${aggregator.limit}};
};