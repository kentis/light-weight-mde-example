const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const AWS = require('aws-sdk');

const aggregator = require('./lib/aggregator') 
<% components.each { %>
const ${it._name} = require('./lib/${it._name}');
<% } %>

app.use(bodyParser.json());

app.post('/score', function (req, res) {
  let scores = { };
  <% components.each { %>
    scores.${it._name} = ${it._name}.score(req.body);
  <% } %>
  
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.send({"details": scores, "result": aggregator.aggregate(scores)});
})

module.exports.handler = serverless(app);