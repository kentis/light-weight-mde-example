package generator

import groovy.text.GStringTemplateEngine;
import groovy.text.SimpleTemplateEngine
import groovy.text.Template

class Generator extends AbstractGenerator {

    def writeAll(path, scoring){
        def base = new File(path)
        base.mkdirs()
        def libs = new File(base, "lib")
        libs.mkdirs()

        writeIndex(base, scoring)
        writeAggregator(libs, scoring)
        writePackage(base)
        writePackageLock(base)
        scoring.components.each{ writeComponent(libs, it)}
        writeServerlessYaml(base, scoring)
    }

    def writeAggregator(path, scoring) {
        def file = new File(path, "aggregator.js")
        file.text = generateAggregator(scoring)
    }

    def generateAggregator(scoring){
        def aggregator = "aggregator"
        switch(scoring.aggregator.type){
            case "WeightedAverageAccumulator":
                aggregator = "aggregator"
                break;
            case "CustomAggregator":
                aggregator = "customAggregator"
                break;
            default:
                throw new Exception("Unknown aggregator ${scoring.aggregator.type}")
        }
        def tmpl = this.class.getResourceAsStream("/templates/${aggregator}.js.gsp").text
        runTemplate(tmpl, scoring)
    }

    def writeIndex(path, scoring){
        def file = new File(path, "index.js")
        file.text = generateIndex(scoring)
    }

    def generateIndex(scoring){
        def tmpl = this.class.getResourceAsStream("/templates/index.js.gsp").text
        runTemplate(tmpl, scoring)
    }

    def writePackage(path){
        def file = new File(path, "package.json")
        file.text = generatePackage()
    }

    def generatePackage(){
        this.class.getResourceAsStream("/templates/package.json").text
    }

    def writePackageLock(path){
        def file = new File(path, "package-lock.json")
        file.text = generatePackageLock()
    }

    def generatePackageLock(){
        this.class.getResourceAsStream("/templates/package-lock.json").text
    }

    def writeComponent(path, component){
        
        def file = new File(path, component._name+".js")
        file.text = generateComponent(component)
    }

    def generateComponent(component){
        this."generate${component.type}Component"(component)
    }
    
    def generateValueMapComponent(component) {
        def tmpl = this.class.getResourceAsStream("/templates/valueMapComponent.js.gsp").text
        runTemplate(tmpl, component)
    }

    def generateSumComponent(component) {
        def tmpl = this.class.getResourceAsStream("/templates/sumComponent.js.gsp").text
        runTemplate(tmpl, component)
    }

    def generateRatioWithLimitComponent(component) {
        def tmpl = this.class.getResourceAsStream("/templates/ratioWithLimitComponent.js.gsp").text
        runTemplate(tmpl, component)
    }

    def generateSetSelectionComponent(component) {
        def tmpl = this.class.getResourceAsStream("/templates/setSelectionComponent.js.gsp").text
        runTemplate(tmpl, component)
    }

    def writeServerlessYaml(path, scoring){
        def file = new File(path, "serverless.yaml")
        file.text = generateServerlessYaml(scoring)
    }

    def generateServerlessYaml(scoring){
        def tmpl = this.class.getResourceAsStream("/templates/serverless.yaml.gsp").text
        runTemplate(tmpl, scoring)
    }


}