package generator

import java.io.File
import org.yaml.snakeyaml.Yaml

class Parser {

    def parse(filename) {
        def yaml = new Yaml()
        def res = yaml.load(new File(filename).text)

        res.scoring.components.each{
            it._name = it.name.toLowerCase().replaceAll(" ", "_")
        }
        return res
    }

}