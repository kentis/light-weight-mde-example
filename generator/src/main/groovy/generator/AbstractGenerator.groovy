package generator

import groovy.text.GStringTemplateEngine;
import groovy.text.SimpleTemplateEngine
import groovy.text.Template

abstract class AbstractGenerator {

    abstract writeAll(path, scoring);


    /**
	 * Runs a template
	 * @param template
	 * @param params
	 * @return
	 */
	def runTemplate(tmpl, params){
		def retval = null
		try{
			//def tmpl = new File(template).text
		
			SimpleTemplateEngine engine = new SimpleTemplateEngine()
			Template simpleTemplate = engine.createTemplate(tmpl)
			retval = simpleTemplate.make(params).toString()
		} catch(Exception e){
			throw new RuntimeException("Exception running template: $tmpl with $params", e)
		}
		return retval
    }
}